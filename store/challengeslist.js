export const state = () => ({
    published:[
        { 
            id: 'maze', 
            link: 'challenges/maze', 
            title: 'Maze'
        },
        { 
            id: 'video-sensing', 
            link: 'challenges/video-sensing', 
            title: 'Video Sensing'
        },
        { 
            id: 'catch-the-egg', 
            link: 'challenges/catch-the-egg', 
            title: 'Catch the Egg'
        },
        { 
            id: 'hat-landing', 
            link: 'challenges/hat-landing', 
            title: 'Hat Landing'
        }
    ],

    featured:[
        { 
            id: 'maze', 
            link: 'challenges/maze', 
            title: 'Maze'
        },
        { 
            id: 'video-sensing', 
            link: 'challenges/video-sensing', 
            title: 'Video Sensing'
        },
        { 
            id: 'catch-the-egg', 
            link: 'challenges/catch-the-egg', 
            title: 'Catch the Egg'
        },
        { 
            id: 'hat-landing', 
            link: 'challenges/hat-landing', 
            title: 'Hat Landing'
        }
    ]
})